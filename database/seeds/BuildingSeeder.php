<?php

use App\Building;
use Illuminate\Database\Seeder;

class BuildingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Mineral mine
        $building = new Building();
        $building->name = 'Mineral mine';
        $building->produce_per_minute = ['mineral' =>  5];
        $building->price = ['mineral' =>  250];

        $building->save();

        // Gas refinery
        $building = new Building();
        $building->name = 'Gas refinery';
        $building->produce_per_minute = ['vespense_gas' =>  5];
        $building->price = ['mineral' =>  750];

        $building->save();
    }
}
