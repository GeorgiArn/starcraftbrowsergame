<?php

use Illuminate\Database\Seeder;

class ResourceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Resource::class)->create(['name' => 'Mineral']);
        factory(\App\Resource::class)->create(['name' => 'Vespene gas']);
    }
}
