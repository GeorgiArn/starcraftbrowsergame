<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Building extends Model
{
    // For setting the json attributes in produce_per_minute & price
    public function setAttribute($key, $value)
    {
        if (in_array($key, ['produce_per_minute', 'price'])) {
            $this->attributes[$key] = json_encode($value, JSON_UNESCAPED_UNICODE);

            return $this;
        }

        // Apply default for everything else
        return parent::setAttribute($key, $value);
    }
}
